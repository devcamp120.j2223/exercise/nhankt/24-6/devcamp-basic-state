import React from "react";

class Count extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            count : this.props.init
        }
    }
    clickBtn=()=>{
        this.setState({count : this.state.count +1})
    }
    render(){
        return(
            <div>
                <p>số lần click : {this.state.count}</p>
                <button onClick={this.clickBtn}>Click me</button>
            </div>
        )
    }
}
export default Count