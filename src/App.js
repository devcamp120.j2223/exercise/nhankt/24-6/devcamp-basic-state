import logo from './logo.svg';
import './App.css';
import Count from './components/Count';

function App() {
  return (
    <div >
      <Count init={0}></Count>
    </div>
  );
}

export default App;
